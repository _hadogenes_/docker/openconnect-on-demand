FROM debian:stable-slim

ENV INOTIFY_TIMEOUT=60

RUN apt update \
 && apt install -y \
        openconnect \
        ocproxy \
        procps \
        inotify-tools \
 && apt clean \
 && rm -rf /var/cache/apt/* \
 && rm -rf /var/lib/apt/lists/*

COPY run.sh /run.sh
RUN chmod 0755 /run.sh

CMD [ "/run.sh" ]

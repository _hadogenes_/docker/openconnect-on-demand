#!/bin/bash

openconnect_args=()
ocproxy_args=()
inotify_args=()

if [ "$VERBOSE" = "1" ]; then
    set -x
    ocproxy_args+=(--verbose)
    openconnect_args+=(--verbose)
else
    inotify_args+=(--quiet --quiet)
fi

OPENCONNECT_PID_FILE="/run/openconect.pid"
REQUEST_DIR=/request

isRequested() {
    [ $(find "$REQUEST_DIR" -mindepth 1 -type f | wc -l) -gt 0 ]
}

doRequest() {
    if isRequested; then
        if [ ! -f "$OPENCONNECT_PID_FILE" ]; then
            echo "Starting openconect"
            echo "$OC_PASSWORD" | openconnect "${openconnect_args[@]}" "$OC_SERVER"
        fi
    else
        if [ -f "$OPENCONNECT_PID_FILE" ]; then
            echo "Stoping openconect"
            cat "$OPENCONNECT_PID_FILE" | xargs --no-run-if-empty kill
        fi
    fi
}

die() {
    echo shutting down
    if [ -f "$OPENCONNECT_PID_FILE" ]; then
        cat "$OPENCONNECT_PID_FILE" | xargs --no-run-if-empty kill
    fi
    exit
}

trap die SIGTERM SIGHUP SIGINT

if [ "$SOCKS_PROXY_PORT" ]; then
    ocproxy_args+=("-D" "$SOCKS_PROXY_PORT")
fi

for localForward in $LOCAL_FORWARD_LIST; do
    ocproxy_args+=("-L" "$localForward")
done

if [ "$OC_USER" ]; then
    openconnect_args+=("--user" "$OC_USER")
fi

if [ "$OC_AUTHGROUP" ]; then
    openconnect_args+=("--authgroup" "$OC_AUTHGROUP")
fi

if [ "$OC_SERVER_CERT" ]; then
    openconnect_args+=("--servercert" "$OC_SERVER_CERT")
fi

ocproxy_args+=(
    --keepalive 30
    --allow-remote
)

ocproxy_cmd="ocproxy ${ocproxy_args[@]}"

openconnect_args+=(
    --pid-file "$OPENCONNECT_PID_FILE"
    --background
    --script-tun
    --script "$ocproxy_cmd"
)

inotify_args+=(
    --event create
    --event delete
    "$REQUEST_DIR"
)

if [ "$INOTIFY_TIMEOUT" ]; then                                                                            │
    inotify_args+=(--timeout "$INOTIFY_TIMEOUT")
fi

while true; do
    doRequest
    inotifywait "${inotify_args[@]}" &
    wait $!
done

